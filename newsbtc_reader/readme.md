#NewsBTC news reader - Documentation files

#1 GENERAL

NewsBTC news reader is a bar that shows news of newsbtc to people who wants
to embeed newsbtc feed on their webpages.

#2 USAGE AND FEATURES

NewsBTC news reader is a html code that must be embeed on the host webpage
like a plugin just with copy and paste the html code, consult the documentation
about [how to setup you own franchise bar](setup_franchise_bar.md).

NewsBTC news reader widget features are:

* show newsbtc last 10 news
* supports consults to multiple franchises
* adapted to it's container

features to develop:

* closed javascript in html code to embeed (hidden html template)
* graphic design
* responsive design
* other layouts, see the [emdyp api](api_reference.md)

###requirements

* modern web browser

#3 ABOUT US

Emdyp.me developed this software, we are a start-up that works with technology and 
language services, visit us at http://emdyp.me, you can write us at [mailbox@emdyp.me](mailto:mailbox@emdyp.me)
or to [latinamerica@newsbtc.com](mailto:latinamerica@newsbtc.com), or follow us at @emdyp.

every comment, help or donation is welcome, you can support us by helping with the development,
designing cool id templates, giving us your feedback.

###Donations
* bitcoin: 1B3RmdZJbR4ArU8MdKL2UPCuacAnRMMmNe

we support bogota coinfest 2015, even in blockchain :) http://www.cryptograffiti.info/?txnr=2238